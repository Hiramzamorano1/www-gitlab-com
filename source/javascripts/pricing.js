const initializeSlider = () => {
  const runInitializeSlider = () => {
    const variant = $('.experiment-visible').hasClass('experiment-test') ? 'test' : 'control';
    $(`.experiment-${variant} .plans`).slick({
      appendDots: `.experiment-${variant} .pricing-nav-mobile`,
      dots: false,
      centerMode: false,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            dots: true,
            initialSlide: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true
          }
        }
      ]
    })
  };
  window.runInitializeSlider = runInitializeSlider;
  runInitializeSlider();
};
window.initializeSlider = initializeSlider;
