---
layout: handbook-page-toc
title: Product Development Budgeting Process
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Pre-planning for potential new investments

The pre-planning is done in advance of the budget becoming available so that the potential investments are well documented and collaboration has occurred on them with the stakeholders.

| DRI | Task | 
| --- | --- | 
| [Product Managers](/job-families/product/product-manager/) | Product managers do their planning for future potential investments in advance.  This will be a list of ready-to-go business cases in advance, using the `potential investment`  issue template. |
| Product Managers | Create private issues using the issue template and collaborate with PM and Engineering teams via tagging the appropriate development director, UX stable counterpart, SET stable counterpart, etc, as appropriate. |

## Vetting and communication of new investment 

| DRI | Task |
| --- | --- | 
| VP of PM | Make sure the funding plan encompasses all known things that need to be budgeted for successfully executing on the opportunity and ensure that the "investment" part of ROI calculation is reflective of the true investment across GitLab as a company | 
| [CTO](https://gitlab.com/edjdev) | Make sure the funding plan encompasses all known things that need to be budgeted from an Engineering perspective (such as not only new hires but comp reviews) | 
| Product Managers | When a hiring plan is approved, document if previously approved reqs are or are not included |
| [VP of PM](https://gitlab.com/adawar) |  Provide a (private) video walkthru of the approved hiring plan (as the spreadsheets tend to be hard to understand without an explanation) |
| Development directors |  The directors will confirm and update the budgeting to ensure it includes proper EM to engineer ratios.  The directors will also collaborate with the leadership from other engineering teams to make sure the plan encompasses proper ratios for UX, SET, SRE, and other stable counterpart roles. |
